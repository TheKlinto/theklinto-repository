﻿// A C# program for Client 
using System;
using System.Net.Sockets;
using System.Text;

namespace Client
{

    class Program
    {

        // Main Method 
        static void Main(string[] args)
        {
            ConnectionSocket connectionSocket = new ConnectionSocket();

            System.ConsoleKey consoleKey = ConsoleKey.Enter;
            while (consoleKey != ConsoleKey.Escape)
            {        
                Console.Write("Besked til server: ");
                connectionSocket.SendMessage(Console.ReadLine());
                Console.WriteLine("\nPress ESC to quit...\n");
                consoleKey = Console.ReadKey().Key;
            }
        }
    }
}
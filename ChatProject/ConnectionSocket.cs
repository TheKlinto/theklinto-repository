﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Client
{
    public class ConnectionSocket
    {
        public ConnectionSocket()
        {
            SetupConnection();
        }
        public IPEndPoint IPEndPoint { get; set; }
        public IPAddress IPAddress { get; set; }
        public IPHostEntry IPHostEntry { get; set; }
        public Socket Socket { get; set; }

        public void SetupConnection()
        {
            this.IPHostEntry = Dns.GetHostEntry(Dns.GetHostName());
            this.IPAddress = IPHostEntry.AddressList[0];
            this.IPEndPoint = new IPEndPoint(this.IPAddress, 11111);

            this.Socket = new Socket(this.IPAddress.AddressFamily,
                       SocketType.Stream, ProtocolType.Tcp);
        }

        public void OpenConnection()
        {
            Socket.Connect(this.IPEndPoint);
        }
        public void CloseConnection()
        {
            Socket.Shutdown(SocketShutdown.Both);
            Socket.Close();
        }

        public void SendMessage(string message)
        {

            OpenConnection();

            // We print EndPoint information  
            // that we are connected 
            Console.WriteLine("Socket connected to -> {0} ",
                          Socket.RemoteEndPoint.ToString());

            // Creation of messagge that 
            // we will send to Server 
            byte[] messageSent = Encoding.ASCII.GetBytes(message);
            int byteSent = Socket.Send(messageSent);

            Console.WriteLine("Message sent");

            // Data buffer 
            byte[] messageReceived = new byte[1024];

            // We receive the messagge using  
            // the method Receive(). This  
            // method returns number of bytes 
            // received, that we'll use to  
            // convert them to string

            CloseConnection();

        }
    }
}

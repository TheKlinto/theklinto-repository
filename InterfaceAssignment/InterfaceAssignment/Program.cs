﻿using System;

namespace InterfaceAssignment
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Clear();
                var veh1 = new Car(1994, 4, 135, "Car", fuel: "Diesel");
                var veh2 = new Bicycle(2017, 0, 2, "Bicycle", "mustang");
                Console.WriteLine($"Vælg et køretøj for mere information" +
                    $"\n[1] {veh1.Type}, {veh1.Brand}" +
                    $"\n[2] {veh2.Type}, {veh2.Brand}");
                Console.Write("Dit valg: ");
                int.TryParse(Console.ReadLine(), out int choice);
                switch (choice)
                {
                    case 1:
                        Console.WriteLine(veh1.Information());
                        break;
                    case 2:
                        Console.WriteLine(veh2.Information());
                        break;
                    default:
                        Console.WriteLine("Intet køretøj valgt");
                        break;
                }

                Console.WriteLine("\nHvis du ønsker at fortsætte tryk på en vilkårlig tast, eller ESC for at afslutte");
            } while (Console.ReadKey().Key != System.ConsoleKey.Escape);
        }
    }
}

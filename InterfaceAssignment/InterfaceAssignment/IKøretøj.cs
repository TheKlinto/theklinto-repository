﻿namespace InterfaceAssignment
{
    public interface IVehicle
    {
        string Brand { get; set; }
        int Year { get; set; }
        int Wheels { get; set; }
        int HP { get; set; }
        string Type { get; set; }
        string Information();
    }
}

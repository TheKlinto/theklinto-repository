﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceAssignment
{
    public class Ballerina
    {
        public string Navn;
        public int Alder = 0;

        public Ballerina(int alder, string navn)
        {
            Navn = navn;
            Alder = alder;
        }
        public Ballerina(int alder) : this (alder, "")
        {

        }
        public static int AntalBallerinaer(List<Ballerina> hold)
        {
            return hold.Count();
        }
        public static string NavnePåHold(List<Ballerina> hold)
        {
            List<Ballerina> list = hold.OrderBy(x => x.Navn).ToList();
            List<string> sorteret = new List<string>();
            foreach (var bal in list)
            {
                if (bal.Navn != "")
                    sorteret.Add(bal.Navn);
                else { }

            }
            return "Ballerinaer på holdet: " + string.Join(", ", sorteret);
        }
        public static string SorterAlder(List<Ballerina> hold)
        {
            List<Ballerina> list = hold.OrderBy(x => x.Alder).ToList();
            List<string> sorteret = new List<string>();
            foreach (var bal in list)
            {
                if (bal.Navn != "")
                    sorteret.Add(bal.Navn);
                else { }

            }
            return "Ballerinaer på holdet: " + string.Join(", ", sorteret);
        }
        public static int SamletAlder(List<Ballerina> hold)
        {
            return hold.Sum(x => x.Alder);
        }
    }
}

﻿namespace InterfaceAssignment
{
    public class Car : IVehicle
    {
        public string Brand { get; set; }
        public int Year { get; set; }
        public string Fuel { get; set; }
        public int Wheels { get; set; }
        public int HP { get; set; }
        public string Type { get; set; }
        public string VIN { get; set; }

        public Car(int year,
            int wheels,
            int hp,
            string type,
            string brand = "no brand",
            string fuel = "not defined")
        {
            Brand = brand;
            Year = year;
            Wheels = wheels;
            HP = hp;
            Fuel = fuel;
            Type = type;
        }
        public string Information()
        {
            return $"Type: {Type} Brand: {Brand}, Year: {Year}, Wheels: {Wheels}, HP: {HP}, Fuel: {Fuel}";
        }
    }
    public class Bicycle : IVehicle
    {
        public string Brand { get; set; }
        public int Year { get; set; }
        public int Wheels { get; set; }
        public int HP { get; set; }
        public string Type { get; set; }
        public string VIN { get; set; }

        public Bicycle(int year, int hp, int wheels, string type, string brand = "No brand")
        {
            Year = year;
            Wheels = wheels;
            Brand = brand;
            HP = hp;
            Type = type;
        }

        public string Information()
        {
            return $"Type: {Type} Brand: {Brand}, Year: {Year}, Wheels: {Wheels}, HP: {HP}";
        }
    }
}

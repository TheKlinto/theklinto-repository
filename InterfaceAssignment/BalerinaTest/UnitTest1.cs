﻿using System;
using System.Collections.Generic;
using InterfaceAssignment;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BalerinaTest
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void AntalBallerinaerTest()
        {
            // Vi vil gerne have en funktion der viser hvor mange ballerinaer der er på et hold
            //Arange
            Ballerina b1, b2, b3, b4, b5, b6, b7;
            List<Ballerina> hold1 = new List<Ballerina>
            {
                (b1 = new Ballerina(11)),
                (b2 = new Ballerina(14)),
                (b3 = new Ballerina(15)),
                (b4 = new Ballerina(19)),
                (b5 = new Ballerina(17)),
                (b6 = new Ballerina(17)),
                (b7 = new Ballerina(12))
            };

            //Act & Assert 
            Assert.AreEqual(7, Ballerina.AntalBallerinaer(hold1));
        }
        [TestMethod]
        public void NavnePåHoldTest()
        {
            // Vi vil gerne have en liste vist med alle ballerinaeres navne i alfabetisk orden, hvor dem uden navne ikke skal vises.
            //Arange
            Ballerina b1, b2, b3, b4, b5, b6, b7;
            List<Ballerina> hold1 = new List<Ballerina>();
            hold1.Add(b1 = new Ballerina(18, "Jens"));
            hold1.Add(b2 = new Ballerina(15));
            hold1.Add(b3 = new Ballerina(21, "Marie"));
            hold1.Add(b4 = new Ballerina(12));
            hold1.Add(b5 = new Ballerina(24, "Anne"));
            hold1.Add(b6 = new Ballerina(12));
            hold1.Add(b7 = new Ballerina(17, "Danny"));

            //Act & Assert 
            Assert.AreEqual("Ballerinaer på holdet: Anne, Danny, Jens, Marie", Ballerina.NavnePåHold(hold1));
        }
        [TestMethod]
        public void SorteretAlder()
        {
            // Holdet vil sorteres efter alder, yngste først, stadig uden personer med intet navn.

            //Arange
            Ballerina b1, b2, b3, b4, b5, b6, b7, b8;
            List<Ballerina> hold1 = new List<Ballerina>();
            hold1.Add(b1 = new Ballerina(18, "Jens"));
            hold1.Add(b2 = new Ballerina(15));
            hold1.Add(b3 = new Ballerina(21, "Marie"));
            hold1.Add(b4 = new Ballerina(13));
            hold1.Add(b5 = new Ballerina(24, "Anne"));
            hold1.Add(b6 = new Ballerina(12));
            hold1.Add(b7 = new Ballerina(17, "Danny"));
            hold1.Add(b8 = new Ballerina(14));

            //Act & Assert
            Assert.AreEqual("Ballerinaer på holdet: Danny, Jens, Marie, Anne", Ballerina.SorterAlder(hold1));
        }

        [TestMethod]
        public void SamletAlder()
        {
            // Vi vil gerne have en int der beskriver alle ballerinaernes alder tilsammen.
            //Arange
            Ballerina b1, b2, b3, b4, b5, b6, b7;
            List<Ballerina> hold1 = new List<Ballerina>();
            hold1.Add(b1 = new Ballerina(18, "Jens"));
            hold1.Add(b2 = new Ballerina(15));
            hold1.Add(b3 = new Ballerina(21, "Marie"));
            hold1.Add(b4 = new Ballerina(12));
            hold1.Add(b5 = new Ballerina(24, "Anne"));
            hold1.Add(b6 = new Ballerina(12));
            hold1.Add(b7 = new Ballerina(17, "Danny"));

            //Assert & Assert
            Assert.AreEqual(119, Ballerina.SamletAlder(hold1));
        }
    }
}

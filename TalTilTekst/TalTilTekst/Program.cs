﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TalTilTekst
{
    class Program
    {
        static void Main(string[] args)
        {
            int number;
            Console.Write("Indtast nummer: ");
            number = int.Parse(Console.ReadLine());
            Console.WriteLine(NumberToText(number));
            Console.Read();

        }

        static string NumberToText(int number)
        {
            int itemsinlist = 0;
            List<int> totalList = new List<int>();
            List<int> enIntlist = new List<int>();
            List<string> enStringlist = new List<string>();
            List<int> tiIntlist = new List<int>();
            List<string> tiStringlist = new List<string>();
            List<int> teenIntlist = new List<int>();
            List<string> teenStringlist = new List<string>();
            enIntlist.Add(0); enStringlist.Add("");
            enIntlist.Add(1); enStringlist.Add("en");
            enIntlist.Add(2); enStringlist.Add("to");
            enIntlist.Add(3); enStringlist.Add("tre");
            enIntlist.Add(4); enStringlist.Add("fire");
            enIntlist.Add(5); enStringlist.Add("fem");
            enIntlist.Add(6); enStringlist.Add("seks");
            enIntlist.Add(7); enStringlist.Add("syv");
            enIntlist.Add(8); enStringlist.Add("otte");
            enIntlist.Add(9); enStringlist.Add("ni");

            tiIntlist.Add(0); tiStringlist.Add("");
            tiIntlist.Add(1); tiStringlist.Add("");
            tiIntlist.Add(2); tiStringlist.Add("tyve");
            tiIntlist.Add(3); tiStringlist.Add("tredive");
            tiIntlist.Add(4); tiStringlist.Add("fyrre");
            tiIntlist.Add(5); tiStringlist.Add("halvtreds");
            tiIntlist.Add(6); tiStringlist.Add("treds");
            tiIntlist.Add(7); tiStringlist.Add("halvfjerds");
            tiIntlist.Add(8); tiStringlist.Add("firs");
            tiIntlist.Add(9); tiStringlist.Add("halvfems");

            teenIntlist.Add(0); teenStringlist.Add("ti");
            teenIntlist.Add(1); teenStringlist.Add("elleve");
            teenIntlist.Add(2); teenStringlist.Add("tolv");
            teenIntlist.Add(3); teenStringlist.Add("tretten");
            teenIntlist.Add(4); teenStringlist.Add("fjorten");
            teenIntlist.Add(5); teenStringlist.Add("femtem");
            teenIntlist.Add(6); teenStringlist.Add("seksten");
            teenIntlist.Add(7); teenStringlist.Add("sytten");
            teenIntlist.Add(8); teenStringlist.Add("atten");
            teenIntlist.Add(9); teenStringlist.Add("nitten");

            string message = number.ToString();
            string tal = "";

            //Max string = {et, hundrede, og, fem, og, halvfems, milioner, et, hundrede, og, fem, og halvfems, tusinde, et, hundrede, og, fem, og, halvfems}
            //Max string = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19}
            List<string> sluttal = new List<string>();

            foreach (char item in message) //Tjekker hvor mange char der er i input
            {
                totalList.Add(int.Parse(item.ToString()));
                itemsinlist++; //tæller op til 9
            }
            for (int i = itemsinlist; i < 9; i++) //tilføjer 0'er foran indtil den rammer 9
            {
                totalList.Insert(0, 0);
            }

            //Milioner - GODKENDT
                foreach (var item in enIntlist) //Checker for plads 00x,000,000 (0)
                {
                    if (totalList[0] == 0 && totalList[1] == 0 && totalList[2] == 0)
                {
                    sluttal.Add("");
                    sluttal.Add("");
                    sluttal.Add("");
                    break;
                }
                    else if (totalList[0] == item && totalList[0] > 1)
                    {
                    tal += enStringlist[item] + " hundrede ";
                    sluttal.Add(enStringlist[item]);
                    sluttal.Add("hundrede");
                    break;
                    }
                    else if (totalList[0] == item && totalList[0] == 1)
                    {
                        tal += "et hundrede ";
                    break;
                    }
                }
                foreach (var item in enIntlist) //Checker for plads 00x,000,000 (2)
                {
                    if (totalList[0] == 0 && totalList[1] == 0 && totalList[2] == 0)
                {
                    break;
                }
                    else if (totalList[2] == item && totalList[1] > 1)
                    {
                        tal += enStringlist[item] + " og ";
                    break;
                    }
                    else if (totalList[1] == 0)
                    {
                        tal += enStringlist[item] + " milioner ";
                    break;
                    }
                     else if (totalList[2] == item && totalList[1] == 1) // Specielle tal
                    {
                        tal += teenStringlist[item] + " milioner ";
                    break;
                    }
                }
                foreach (var item in enIntlist) //Checker for plads 0x0,000,000 (1)
                {
                if (totalList[0] == 0 && totalList[1] == 0 && totalList[2] == 0)
                {
                    break;
                }
                else if (totalList[1] == item && totalList[1] > 1)
                    {
                        tal += tiStringlist[item] + " milioner ";
                    break;
                    }
                }
            //Tusinde - GODKENDT
            foreach (var item in enIntlist) //Checker for plads 000,x00,000 (3)
            {
                if (totalList[3] == 0 && totalList[4] == 0 && totalList[5] == 0)
                {
                    break;
                }
                else if (totalList[3] == item && totalList[3] > 1)
                {
                    tal += enStringlist[item] + " hundrede ";
                    break;
                }
                else if (totalList[3] == item && totalList[3] == 1)
                {
                    tal += " et hundrede ";
                    break;
                }
            }
            foreach (var item in enIntlist) //Checker for plads 000,00x,000 (5)
            {
                if (totalList[3] == 0 && totalList[4] == 0 && totalList[5] == 0)
                {
                    break;
                }
                else if (totalList[5] == item && totalList[4] > 1)
                {
                    tal += enStringlist[item] + " og ";
                    break;
                }
                else if (totalList[4] == 0)
                {
                    tal += enStringlist[item] + " tusinde ";
                    break;
                }
                else if (totalList[5] == item && totalList[4] == 1) // Specielle tal
                {
                    tal += teenStringlist[item] + " tusinde ";
                    break;
                }
            }
            foreach (var item in enIntlist) //Checker for plads 000,0x0,000 (4)
            {
                if (totalList[3] == 0 && totalList[4] == 0 && totalList[5] == 0)
                {
                    break;
                }
                else if (totalList[4] == item && totalList[4] > 1)
                {
                    tal += tiStringlist[item] + " tusinde ";
                    break;
                }
            }
            //Hundrede - 
            foreach (var item in enIntlist) //Checker for plads 000,000,x00 (6)
            {
                if (totalList[6] == 0 && totalList[7] == 0 && totalList[8] == 0)
                {
                    break;
                }
                if (totalList[6] > 1)
                {
                    tal += enStringlist[item] + " hundrede ";
                    break;
                }
                else if (totalList[6] == 1)
                {
                    tal += "et hundrede ";
                    break;
                }
                else if (totalList[6] == 0)
                    break;
            }
            foreach (var item in enIntlist) //Checker for plads 000,000,00x (8)
            {
                if (totalList[8] == 0)
                    break;
                if (totalList[7] > 1)
                {
                    tal += enStringlist[item] + "og ";
                    break;
                }
                else if (totalList[7] == 0)
                {
                    tal += enStringlist[item] + " ";
                    break;
                }
                else if (totalList[7] == 1) // Specielle tal
                {
                    tal += teenStringlist[item] + " ";
                    break;
                }
            }
            foreach (var item in enIntlist) //Checker for plads 000,000,0x0 (7)
            {
                if (totalList[7] == item && totalList[7] > 1)
                {
                    tal += tiStringlist[item];
                    break;
                }
            }
            return tal;
        }
    }
}

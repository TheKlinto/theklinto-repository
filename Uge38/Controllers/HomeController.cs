﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Uge38.Models;
using Microsoft.EntityFrameworkCore;

namespace Uge38.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UserDataContext _db;

        public HomeController(ILogger<HomeController> logger, UserDataContext db)
        {
            _logger = logger;
            _db = db;
        }

        public IActionResult Index()
        {
            var users = _db.Users.ToArray();
            return View(users);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateUser(User user)
        {
            user.IsAdmin = false;

            _db.Users.Add(user);
            _db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult EditUser(int id)
        {
            User user = _db.Users.ToArray().FirstOrDefault(x => x.Id == id);

            return View(user);
        }
        [HttpPost]
        public IActionResult EditUser(User user)
        {
            _db.Users.Update(user);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

﻿using System;
using System.Collections.Generic; 
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Uge38.Models
{
    public class UserDataContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public UserDataContext(DbContextOptions<UserDataContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}

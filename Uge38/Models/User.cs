﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Uge38.Models
{
    public class User
    {
        public long Id { get; set; }

        private string _key;
        public string Key
        {
            get
            {
                if (_key == null)
                    _key = Email.ToLower();
                return _key;
            }
            set
            {
                _key = value;
            }
        }


        [Required]
        public string Name { get; set; }
        [Required, RegularExpression("^(?=.*[A-Z])(?=.*[1-9]).{8,}$",
            ErrorMessage = "Mindst 8 tegn, 1 stort bogstav og 1 tal for adgangskoden")]
        public string Password { get; set; }
        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public bool IsAdmin { get; set; }
        [Required]
        public string Address { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class User
    {
        public User(string firstname, string lastname, string email)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Email = email;
        }

        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            return $"{Firstname}\t{Lastname}\t{Email}";
        }
    }
}

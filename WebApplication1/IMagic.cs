﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public interface IMagic
    {
        public string DoMagic(string str);
    }

    public class MagicA : IMagic
    {
        public string DoMagic(string str)
        {
            char[] arr = str.ToArray();
            Array.Reverse(arr);
            return new string(arr);
        }
    }

    public class MagicB : IMagic
    {
        public string DoMagic(string str)
        {
            return str.ToUpper();
        }
    }
}

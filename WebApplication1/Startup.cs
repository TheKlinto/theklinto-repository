using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using IHostingEnvironment = Microsoft.Extensions.Hosting.IHostingEnvironment;

namespace WebApplication1
{
    public class Startup
    {
        private readonly IConfiguration configuration;
        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddTransient<IMagic, MagicA>();
            services.AddTransient<IMagic, MagicB>();
            services.AddSingleton<Users>();
            services.AddSingleton<Features>();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IMagic magic, IServiceProvider service, Users users, Features features)
        {
            var a = service.GetServices<IMagic>();
            var b = a.First(x => x.GetType() == typeof(MagicA));

            if (env.IsDevelopment())
            {
                b = a.First(x => x.GetType() == typeof(MagicB));
                app.UseDeveloperExceptionPage();
                app.Run(async (context) =>
                {
                    await context.Response.WriteAsync(b.DoMagic("Hello World!"));
                });
            }

            else if (features.CustomData)
            {
                app.Run(async (context) =>
                {
                    await context.Response.WriteAsync(
                        (users.users.Find(x => x.Firstname == "Peter").ToString()) + "\n" +
                        (users.users.Find(x => x.Firstname == "Hans").ToString()) + "\n" +
                        (users.users.Find(x => x.Firstname == "Sten").ToString())
                        );
                });
            }

            else
            {
                app.Run(async (context) =>
                {
                    await context.Response.WriteAsync(b.DoMagic("Hello World!"));
                });

            }

        }

    }
}

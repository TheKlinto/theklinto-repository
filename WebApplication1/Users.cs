﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class Users
    {
        public List<User> users = new List<User>();
        public Users()
        {
            users.Add(new User("Sten", "Hansen", "StenHansen@gmail.com"));
            users.Add(new User("Peter", "Jensen", "PeterJensen@gmail.com"));
            users.Add(new User("Hans", "Jørgsensen", "HansJørgensen@gmail.com"));
    }
    }
}

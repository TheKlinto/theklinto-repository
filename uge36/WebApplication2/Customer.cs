﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace WebApplication2
{
    public class Customer
    {
        public Customer(string firstname, string lastname, string email)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Email = email;
        }
        public Customer()
        {

        }

        [StringLength(15)]
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        [BindNever]
        public bool IsAdmin { get; set; }

    }
}

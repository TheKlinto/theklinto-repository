﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2
{
    public class CustomerRepository
    {
        public CustomerRepository()
        {
            customers.Add(new Customer("Peter", "Hansen", "Peterhansen@gmail.com"));
            customers.Add(new Customer("Lene", "Jensen", "LeneJensen@gmail.com"));
            customers.Add(new Customer("Hanna", "Petersen", "Hannepetersen@gmail.com"));
            customers.Add(new Customer("Jens", "Mogensen", "Jensmogensen@gmail.com"));
        }

        public List<Customer> customers = new List<Customer>();

    }
}

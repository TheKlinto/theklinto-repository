﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly CustomerRepository _customerRepository;
        public HomeController(ILogger<HomeController> logger, CustomerRepository customerRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
        }
        public List<Customer> Index()
        {
            return _customerRepository.customers;
        }

        [Route("[Controller]/Customer/{firstname?}")]
        public List<Customer> Customer(string firstname)
        {
            return _customerRepository.customers.FindAll(x => x.Firstname.StartsWith(firstname.ToUpper()));
        }

        [HttpPost]
        public Customer Index([FromBody]Customer customer)
        {
            _customerRepository.customers.Add(customer);
            return customer;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

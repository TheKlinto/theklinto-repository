﻿using System.Collections.Generic;
using System.Data.SQLite;


namespace SimpleQuery
{
    public class SQL
    {
        public List<Dictionary<string, object>> Query(string query, params object[] obj)
            /*
                Example of use:
                SQL.Query("SELECT * FROM @0 WHERE @1 = @2", obj0 obj1 obj2)
                Use @n for every variable, and remember to add objects in the right order.
            */
        {
            List<Dictionary<string, object>> result = new List<Dictionary<string, object>>(); //Creates a result list the method can add results to.

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString)) //Create a temperary variable holding the connectionstring, to ensure closing after method is run.
            {
                conn.Open(); //Opens the connection

                SQLiteCommand cmd = new SQLiteCommand(query, conn); //Opens a command connection.

                for (int i = 0; i < obj.Length; i++)
                    cmd.Parameters.AddWithValue($"@{i}", obj[i]); //Uses the SQLite function to add our objects to the query string one by one.

                SQLiteDataReader reader = cmd.ExecuteReader(); //Opens the reader for returning results after query is sent to database

                while (reader.Read())
                {
                    Dictionary<string, object> dict = new Dictionary<string, object>();    //Creates a dictionary contaning the (string)column name, and the (object)field value
                                                                    //In this case the Key is the string and the value is the object.

                    for (int field = 0; field < reader.FieldCount; field++)
                        dict[reader.GetName(field)] = reader.GetValue(field); //Finds all the keys and values of the entries found in the database

                    result.Add(dict); //Adds the keys and values to the dictionary
                }
            }

            return result; //Returns the result dictionary
        }

        public string ConnectionString { get { return _connectionString; } } //Makes sure the connectionstring can't be altered after object is created

        private readonly string _connectionString = ""; //The connection string used for connection to database
        //Format: "Data source = database.db; Version = 3; password = myPassword"

        //Constructors to make databasefile 
        public SQL(string database, string password, string version)
        {
            if (password != null)
                _connectionString = $"Data Source = {database}; Version = {version}; password = {password}";
            else
                _connectionString = $"Data Source = {database}; Version = {version}";
            using (SQLiteConnection sql = new SQLiteConnection(_connectionString))
            {
                sql.Open();
                sql.Close();
            }
            return;
        }
        public SQL(string database, string password = "") : this(database, password, "3") { }
        public SQL(string database) : this(database, null, "3") { }


    }
}

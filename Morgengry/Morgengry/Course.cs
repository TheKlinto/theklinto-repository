﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morgengry
{
    public class Course : IValuable
    {
        public string Name { get; set; }
        public int DurationInMinutes { get; set; }
        private double _CourseHourValue = 875;
        public double CourseHourValue
        {
            get
            {
                return _CourseHourValue;
            }
            set
            {
                    _CourseHourValue = value;
            }
        }
        public Course(string name, int duration)
        {
            Name = name;
            DurationInMinutes = duration;
        }
        public Course(string name) : this(name, 0)
        {
        }
        public override string ToString()
        {
            return $"Name: {Name}, Duration in Minutes: {DurationInMinutes}, Pris pr påbegyndt time: {CourseHourValue}";
        }

        public double GetValue()
        {
            return Math.Ceiling((DurationInMinutes / 60.00)) * CourseHourValue;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Morgengry
{
    public class ValuableRepository : IPersistable
    {
        private List<IValuable> valuables = new List<IValuable>();
        public void AddValuable(IValuable valuable)
        {
            valuables.Add(valuable);
        }
        public IValuable GetValuable(string id)
        {
            return valuables.Find(x => (x is Merchandise) && ((Merchandise)x).ItemId == id || (x is Course) && ((Course)x).Name == id);
        }
        public double GetTotalValue()
        {
            return valuables.Sum(x => x.GetValue());
        }
        public int Count()
        {
            return valuables.Count();
        }

        public void Save(string filename = "ValuableRepository.txt")
        {
            StreamWriter save = new StreamWriter(filename);

            foreach (var item in valuables)
            {
                if (item is Book)
                    save.WriteLine(((Book)item).GetType() + ";" + ((Book)item).ItemId + ";" + ((Book)item).Title + ";" + ((Book)item).Price);
                else if (item is Amulet)
                    save.WriteLine(((Amulet)item).GetType() + ";" + ((Amulet)item).ItemId + ";" + ((Amulet)item).Quality + ";" + ((Amulet)item).Design);
                else if (item is Course)
                    save.WriteLine(((Course)item).GetType() + ";" + ((Course)item).Name + ";" + ((Course)item).DurationInMinutes + ";" + ((Course)item).GetValue());
            }
            save.Close();
        }

        public void Load(string filename = "ValuableRepository.txt")
        {
            valuables.Clear();
            StreamReader read = new StreamReader(filename);
            while(read.EndOfStream != false)
            {
                var item = Console.ReadLine().Split(';');
                if (item[0] == "Morgengry.Book")
                {
                    Book book = new Book(item[2], item[3], Convert.ToDouble(item[4]));
                    valuables.Add(book);
                }
                else if (item[0] == "Morgengry.Amulet")
                {
                    switch (item[3])
                    {
                        case "low":
                            Amulet a1 = new Amulet(item[2], Level.low, item[4]);
                            valuables.Add(a1);
                            break;
                        case "medium":
                            Amulet a2 = new Amulet(item[2], Level.medium, item[4]);
                            valuables.Add(a2);
                            break;
                        case "high":
                            Amulet a3 = new Amulet(item[2], Level.high, item[4]);
                            valuables.Add(a3);
                            break;
                    }         
                }
                else if (item[0] == "Morgengry.Course")
                {
                    Course course = new Course(item[1], int.Parse(item[2]));
                    valuables.Add(course);
                }
            }
            read.Close();
        }
    }
}

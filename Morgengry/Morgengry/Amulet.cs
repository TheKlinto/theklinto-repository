﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morgengry
{
    public enum Level
    {
        low,
        medium,
        high
    }
    public class Amulet : Merchandise, IValuable
    {
        public static double LowQualityValue = 12.5;
        public static double MediumQualityValue = 20.00;
        public static double HighQualityValue = 27.5;

        public Level Quality { get; set;}
        public string Design { get; set; }
        public Amulet(string itemId, Level quality, string design)
        {
            ItemId = itemId;
            Quality = quality;
            Design = design;
        }
        public Amulet(string itemId, Level quality) : this (itemId, quality, "")
        {
        }
        public Amulet(string itemId) : this (itemId, Level.medium, "")
        {
        }
        public override string ToString()
        { 
            return $"ItemId: {ItemId}, Quality: {Quality}, Design: {Design}";
        }

        public double GetValue()
        {
            switch (Quality)
            {
                case Level.low:
                    return LowQualityValue;
                case Level.medium:
                    return MediumQualityValue;
                case Level.high:
                    return HighQualityValue;
                default:
                    throw new IndexOutOfRangeException("Quality out of range");
            }
        }
    }
}

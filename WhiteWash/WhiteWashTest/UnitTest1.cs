﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WhiteWash;

namespace WhiteWashTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAccount1()
        {
            // Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(10000);
            acc.Deposit(2500);
            acc.Draw(4500);

            // Assert
            Assert.AreEqual(8000, acc.Balance);
        }

        [TestMethod]
        public void TestAccount2()
        {
            // Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(10000);
            acc.Deposit(2500);
            acc.Draw(4500);

            // Assert
            Assert.AreEqual(true, acc.Balance >= 0.0);
        }

        [TestMethod]
        public void TestAccount3()
        {
            // Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(2000);
            acc.Deposit(2500);
            acc.Draw(4500);

            // Assert
            Assert.AreEqual(0.0, acc.Balance);
        }

        [TestMethod]
        public void TestAccount4()
        {
            // Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(2000);
            acc.Deposit(2500);
            acc.Draw(4500);

            // Assert
            Assert.AreEqual(true, acc.Balance >= 0.0);
        }

        [TestMethod]
        public void TestAccount5()
        {
            // Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(1000);
            acc.Deposit(2500);
            acc.Draw(4500);

            // Assert
            Assert.AreEqual(0.0, acc.Balance);
        }

        [TestMethod]
        public void TestAccount6()
        {
            // Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(1000);
            acc.Deposit(2500);
            acc.Balance -= 4500;

            // Assert
            Assert.AreEqual(true, acc.Balance >= 0.0);
        }

        [TestMethod]
        public void TestAccount7()
        {
            // Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(1000);
            acc.Deposit(2500);

            // Assert
            Assert.AreEqual(3500.0, acc.Draw(4500));
        }
    }
}

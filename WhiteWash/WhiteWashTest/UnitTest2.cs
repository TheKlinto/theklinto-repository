﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WhiteWash;
using static WhiteWash.Account;

namespace WhiteWashTest
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void TestSalaryAccount()
        {
            //Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(3000);
            acc.Draw(4000);
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(2000, acc.Balance);
        }
        [TestMethod]
        public void TestSalaryAccount2()
        {
            //Arrange
            Account acc = new Account();
            acc.type = AccountType.Salary;

            // Act
            acc.Deposit(3000);
            acc.Draw(4000);
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(2000, acc.Balance);
        }
        [TestMethod]
        public void TestSalaryAccount3()
        {
            //Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(3000);
            acc.Draw(4000);
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(2000, acc.Draw(2500));
        }
        [TestMethod]
        public void TestSavingsAccount()
        {
            //Arrange
            Account acc = new Account();
            acc.type = AccountType.Savings;

            // Act
            acc.Deposit(3000);
            acc.Draw(4000);
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(5000, acc.Balance);
        }
        [TestMethod]
        public void TestSavingsAccount2()
        {
            //Arrange
            Account acc = new Account();
            acc.type = AccountType.Savings;

            // Act
            acc.Deposit(3000);
            acc.Draw(4000);
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(0, acc.Draw(1500));
        }
        [TestMethod]
        public void TestLoanAccount()
        {
            //Arrange
            Account acc = new Account();
            acc.type = AccountType.Loan;

            // Act
            acc.Deposit(3000);
            acc.Draw(4000);
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(-2000, acc.Balance);
        }
        [TestMethod]
        public void TestLoanAccount2()
        {
            //Arrange
            Account acc = new Account();
            acc.type = AccountType.Loan;

            // Act
            acc.Deposit(3000);
            acc.Draw(4000);
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(2000, acc.Deposit(2500));
        }
        [TestMethod]
        public void TestLoanAccount3()
        {
            //Arrange
            Account acc = new Account();
            acc.type = AccountType.Loan;

            // Act
            acc.Deposit(3000);
            acc.Draw(4000);
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(1500, acc.Deposit(1500));
        }

    }
}

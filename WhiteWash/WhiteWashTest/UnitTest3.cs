﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WhiteWash;
using static WhiteWash.Account;

namespace WhiteWashTest
{
    [TestClass]
    public class UnitTest3
    {
        [TestMethod]
        public void TestNewAccount()
        {
            //Arrange
            Account acc = new Account();

            // Assert
            Assert.AreEqual(AccountState.New, acc.status);
        }
        [TestMethod]
        public void TestNewAccount2()
        {
            //Arrange
            Account acc = new Account();

            // Act
            acc.Lock();

            // Assert
            Assert.AreEqual(AccountState.New, acc.status);
        }
        [TestMethod]
        public void TestNewAccount3()
        {
            //Arrange
            Account acc = new Account();

            // Act
            acc.Lock();
            acc.Unlock();

            // Assert
            Assert.AreEqual(AccountState.New, acc.status);
        }
        [TestMethod]
        public void TestInUseAccount()
        {
            //Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(3000);
            acc.Draw(4000);
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(AccountState.InUse, acc.status);
        }
        [TestMethod]
        public void TestLockedAccount()
        {
            //Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(3000);
            acc.Lock();
            acc.Draw(4000);
            acc.Unlock();
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(5000, acc.Balance);
        }
        [TestMethod]
        public void TestLockedAccount2()
        {
            //Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(3000);
            acc.Lock();
            acc.Draw(4000);
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(AccountState.Locked, acc.status);
        }
        [TestMethod]
        public void TestLockedAccount3()
        {
            //Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(3000);
            acc.Lock();
            acc.Draw(4000);
            acc.Unlock();
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(AccountState.InUse, acc.status);
        }
        [TestMethod]
        public void TestClosedAccount()
        {
            //Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(3000);
            acc.Lock();
            acc.Draw(4000);
            acc.Unlock();
            acc.Deposit(2000);

            // Assert
            Assert.AreEqual(false, acc.Close());
        }
        [TestMethod]
        public void TestClosedAccount2()
        {
            //Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(3000);
            acc.Lock();
            acc.Draw(4000);
            acc.Unlock();
            acc.Draw(3000);

            // Assert
            Assert.AreEqual(true, acc.Close());
        }
        [TestMethod]
        public void TestClosedAccount3()
        {
            //Arrange
            Account acc = new Account();

            // Assert
            Assert.AreEqual(false, acc.Close());
        }
        [TestMethod]
        public void TestClosedAccount4()
        {
            //Arrange
            Account acc = new Account();

            // Act
            acc.Deposit(3000);
            acc.Lock();

            // Assert
            Assert.AreEqual(false, acc.Close());
        }
    }
}

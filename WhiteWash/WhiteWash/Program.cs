﻿using System;
using System.Collections.Generic;

namespace WhiteWash
{
    class Program
    {
        static void Main(string[] args)
        {
            Account acc = new Account();
            acc.Deposit(2000);
            acc.Deposit(2000);
            acc.Deposit(2000);
            acc.Deposit(2000);
            acc.Deposit(2000);
            acc.Deposit(2000);
            acc.Deposit(2000);
            acc.Deposit(2000);
            acc.Draw(1500);
            acc.Close();
            acc.Lock();
            acc.Unlock();
            acc.ShowHistory();
            Console.Read();

        }
    }
}

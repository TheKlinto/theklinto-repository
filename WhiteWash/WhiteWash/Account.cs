﻿using System.Collections.Generic;
using System;
namespace WhiteWash
{
    public class Account
    {
        public enum AccountType
        {
            Salary,
            Savings,
            Loan,
        }
        public enum AccountState
        {
            New,
            InUse,
            Locked,
            Closed,

        }

        public List<string> history = new List<string>(); // Lagre de alle beskeder
        
        
        public void ShowHistory()
        {
            List<string> showhistory = new List<string>(); // Lagre de 10 sidste beskeder
            if (history.Count < 10)
            {
                showhistory = history;
            }
            else
            {
                int story = history.Count;
            }
            string result = string.Join("\n", showhistory);
            Console.WriteLine(result);
        }

        public AccountState status = AccountState.New; //Deklærere statuts på konto til ny
        public AccountType type = AccountType.Salary; //Deklærere kontotype til lønkonto

        private double balance = 0; //Backing store
        public double Balance
        {
            get
            {
                return balance;
            }
            set
            {
                switch (type)
                {
                    case AccountType.Loan:
                        {
                            if (value < 0)
                            {
                                balance = value;
                                break;
                            }
                            else
                            {
                                balance = 0;
                                break;
                            }
                        }
                    case AccountType.Salary:
                        {
                            if (value >= balance)
                            {
                                balance = value;
                                break;
                            }
                            else
                            {
                                balance = 0;
                                break;
                            }
                        }
                    case AccountType.Savings:
                        {
                            if (value >= balance)
                            {
                                balance = value;
                                break;
                            }
                            else
                                balance = 0;
                            break;
                        }
                }

            }
        }
        public bool Close()
        {
            if (status == AccountState.InUse && balance == 0)
            {
                status = AccountState.Closed;
                history.Add("\nAccount Closed" +
                    "\nBalance: " + Balance.ToString() +
                    "\nAccount status: " + status.ToString());
                return true;
            }
            else
            {
                history.Add("\nAccount closing failed" +
                    "\nBalance: " + Balance.ToString() +
                    "\nAccount status: " + status.ToString());
                return false;
            }
        }
        public bool Lock()
        {
            if (status == AccountState.InUse)
            {
                status = AccountState.Locked;
                history.Add("\nAccount locked" +
                    "\nBalance: " + Balance.ToString() +
                    "\nAccount status: " + status.ToString());
                return true;
            }
            else
            {
                history.Add("\nAccount locking failed" +
                    "\nBalance: " + Balance.ToString() +
                    "\nAccount status: " + status.ToString());
                return false;
            }
        }
        public bool Unlock()
        {
            if (status == AccountState.Locked)
            {
                status = AccountState.InUse;
                history.Add("\nAccount Unlocked" +
                    "\nBalance: " + Balance.ToString() +
                    "\nAccount status: " + status.ToString());
                return true;
            }
            else
            {
                history.Add("\nAccount unlocking failed" +
                    "\nBalance: " + Balance.ToString() +
                    "\nAccount status: " + status.ToString());
                return false;
            }
        }


        public double Draw(double amount)
        {
            if (status == AccountState.InUse | status == AccountState.New)
            {
                status = AccountState.InUse;
                switch (type)
                {
                    case AccountType.Loan:
                        {
                            if (amount > 0)
                            {
                                balance -= amount;
                                history.Add("\nWithdrawel: " + amount.ToString() +
                                    "\nBalance: " + Balance.ToString() +
                                    "\nAccount status: " + status.ToString());
                                return amount;
                            }
                            else
                            {
                                balance += amount;
                                history.Add("\nWithdrawel: " + amount.ToString() +
                                    "\nBalance: " + Balance.ToString() +
                                    "\nAccount status: " + status.ToString());
                                return amount;
                            }
                        }
                    case AccountType.Savings:
                        {
                            return 0;
                        }
                    case AccountType.Salary:
                        {
                            if (amount <= balance)
                            {
                                balance -= amount;
                                history.Add("\nWithdrawel: " + amount.ToString() +
                                     "\nBalance: " + Balance.ToString() +
                                     "\nAccount status: " + status.ToString());
                                return amount;
                            }
                            else
                            {
                                double beforedraw = Balance;
                                balance -= balance;
                                history.Add("\nWithdrawel: " + beforedraw.ToString() +
                                    "\nBalance: " + Balance.ToString() +
                                    "\nAccount status: " + status.ToString());
                                return beforedraw;
                            }
                        }
                }
                return 0;
            }
            else
                return 0;
        }
        public double Deposit(double amount)
        {
            if (status == AccountState.New | status == AccountState.InUse)
            {
                status = AccountState.InUse;
                switch (type)
                {
                    case AccountType.Loan:
                        {
                            if (balance + amount > 0)
                            {
                                double a = balance;
                                balance -= balance;
                                history.Add("\nDeposit: " + (-a).ToString() +
                                    "\nBalance: " + Balance.ToString() +
                                    "\nAccount status: " + status.ToString());
                                return -(a);
                            }
                            else if (amount + balance < 0)
                            {
                                balance += amount;
                                history.Add("\nDeposit: " + amount.ToString() +
                                    "\nBalance: " + Balance.ToString() +
                                    "\nAccount status: " + status.ToString());
                                return amount;

                            }
                            break;
                        }
                    case AccountType.Salary:
                        {
                            if (amount > 0)
                            {
                                balance += amount;
                                history.Add("\nDeposit: " + balance.ToString() +
                                    "\nBalance: " + Balance.ToString() +
                                    "\nAccount status: " + status.ToString());
                                return balance;
                            }
                            break;
                        }
                    case AccountType.Savings:
                        {
                            if (amount > 0)
                            {
                                balance += amount;
                                history.Add("\nDeposit: " + balance.ToString() +
                                    "\nBalance: " + Balance.ToString() +
                                    "\nAccount status: " + status.ToString());
                                return balance;
                            }
                            break;
                        }
                }
                return 0;

            }
            else
            {
                return 0;
            }
        }
    }
}

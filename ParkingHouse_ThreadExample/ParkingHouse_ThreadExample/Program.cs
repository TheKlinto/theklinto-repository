﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace ParkingHouse_ThreadExample
{
    class Program
    {
        private static List<Car> queue = new List<Car>();
        private static List<Car> parked = new List<Car>(10);
        private static Random rnd = new Random();
        static void Main(string[] args)
        {
            
            queue = GenerateCars(50);
            Thread thread1 = new Thread(EnteringThread);
            thread1.Start();
            Thread thread2 = new Thread(LeavingThread);
            thread2.Start();

            Console.ReadLine();
        }


        public static void EnteringThread()
        {
            while (queue.Count != 0)
            {
                Thread.Sleep(rnd.Next(0, 500));
                Enter();
            }
            Console.WriteLine("Entering Queue Empty");
        }

        public static void LeavingThread()
        {
            while (queue.Count != 0 || parked.Count > 0)
            {
                Thread.Sleep(rnd.Next(500, 1000));
                Leave();
            }
            Console.WriteLine("Parking House Empty");
        }

        public static void Enter()
        {
            Monitor.Enter(queue);
            if (parked.Count < 10)
            {
                Monitor.Enter(parked);
                int number = rnd.Next(0, queue.Count);
                Console.WriteLine($"Is Entering: {queue[number].Name}");
                parked.Add(queue[number]);
                queue.RemoveAt(number);
                Monitor.PulseAll(parked);
                Monitor.Exit(parked);
                
            }
            Monitor.Exit(queue);

        }
        public static void Leave()
        {
            Monitor.Enter(parked);
            if(parked.Count != 0)
            {
                int number = rnd.Next(0, parked.Count);
                Console.WriteLine($"Is Leaving: {parked[number].Name}");
                parked.RemoveAt(number);
                Monitor.PulseAll(parked);
                Monitor.Exit(parked);
            }
        }

        public static List<Car> GenerateCars(int amount)
        {
            List<Car> res = new List<Car>();
            for(int i = 0; i < amount; i++)
            {
                res.Add(new Car($"Car_{i}"));
            }

            return res;
        }
    }
}
